# Description
A simple GTK app for controlling my lights from my PinePhone.

It communicates with my lighting control system using Modbus to simulate light-switch presses.

Lighting controller source: https://gitlab.com/shiftee1/smartlightingcontroller


# Dependencies
sudo apt install meson libgtk-4-dev libmodbus-dev


# Build instructions

```bash
meson setup build
cd build
meson compile
meson install
```

