#include "portscan.h"

#include <stdio.h>
#include <gtk/gtk.h>
#include <modbus/modbus.h>

/* global variables */
char     g_ip_addr_controller[16];

typedef enum
{
	INPUT_KITCHEN_SPOTS,
	INPUT_KITCHEN_ISLAND,
	INPUT_KITCHEN_DINING,
	INPUT_KITCHEN_WALL,
	INPUT_UTILITY,
	INPUT_UNUSED_1,
	INPUT_OFFICE,
	INPUT_UNUSED_2,
	INPUT_LIVING_ROOM_CABINET,
	INPUT_LIVING_ROOM_WALL,
	INPUT_LIVING_ROOM_SPOTS,
	INPUT_ENTRANCE_HALL_WALL,
	INPUT_ENTRANCE_HALL_LIGHT,
	INPUT_HALLWAY_WALL,
	INPUT_HALLWAY_SPOTS,
	INPUT_UNUSED_3,
	INPUT_UNUSED_4,
	INPUT_MOTION,
	INPUT_LIGHT_SENSOR,
	INPUT_UNUSED_5,
	INPUT_UNUSED_6,
	INPUT_COUNT
} input_t;

const char* get_input_name(input_t input)
{
	switch(input)
	{
	case INPUT_KITCHEN_SPOTS:       return "Kitchen Spots";
	case INPUT_KITCHEN_ISLAND:      return "Kitchen Island";
	case INPUT_KITCHEN_DINING:      return "Kitchen Table";
	case INPUT_KITCHEN_WALL:        return "Kitchen Wall";
	case INPUT_UTILITY:             return "Utility";
	case INPUT_UNUSED_1:            return NULL;
	case INPUT_OFFICE:              return "Office";
	case INPUT_UNUSED_2:            return NULL;
	case INPUT_LIVING_ROOM_CABINET: return "Living Room Cabinets";
	case INPUT_LIVING_ROOM_WALL:    return "Living Room Wall";
	case INPUT_LIVING_ROOM_SPOTS:   return "Living Room Spots";
	case INPUT_ENTRANCE_HALL_WALL:  return "Hall Wall";
	case INPUT_ENTRANCE_HALL_LIGHT: return "Hall Roof";
	case INPUT_HALLWAY_WALL:        return NULL;
	case INPUT_HALLWAY_SPOTS:       return NULL;
	case INPUT_UNUSED_3:            return NULL;
	case INPUT_UNUSED_4:            return NULL;
	case INPUT_MOTION:              return "Motion Sensor";
	case INPUT_LIGHT_SENSOR:        return NULL;
	case INPUT_UNUSED_5:            return NULL;
	case INPUT_UNUSED_6:            return NULL;
	case INPUT_COUNT:               return NULL;
	}

	return NULL;
}

int write_modbus_register(char* p_addr, unsigned port, uint16_t offset, uint16_t val)
{
	modbus_t* mb = modbus_new_tcp(p_addr, port);

	if( ! mb )
	{
		printf("Failed to alloc TCP object\n");
		return -1;
	}

	int status = modbus_connect(mb);

	if( status == 0 )
	{
		status = modbus_write_register(mb, offset, val);

		if( status <= 0 )
			printf("FAILED TO WRITE REGISTER!!\n");

		modbus_close(mb);
	}
	else
	{
		printf("Failed to open TCP connection\n");
	}

	modbus_free(mb);

	return status;
}

int read_modbus_register(char* p_addr, unsigned port, uint16_t offset, uint16_t *val)
{
	modbus_t* mb = modbus_new_tcp(p_addr, port);

	if( ! mb )
	{
		printf("Failed to alloc TCP object\n");
		return -1;
	}

	int status = modbus_connect(mb);

	if( status == 0 )
	{
		status = modbus_read_registers(mb, offset, 1, val);

		if( status == -1 )
			printf("FAILED TO READ REGISTER!!\n");

		modbus_close(mb);
	}
	else
	{
		printf("Failed to open TCP connection\n");
	}

	modbus_free(mb);

	return status;
}

/*
 * Try to detect controller ip address on local network
 *
 * Returns 0 on success
 */
int find_controller_addr(char *p_addr_controller)
{
	struct portscan_data port_data = {0};

	/* find all local ip addresses with an open port */
	int status = portscan(&port_data, 502);
	if ( status )
	{
		return -1;
	}

	/* records the first address where we read the expected project id */
	status = -1;
	int addr_detected = 0;
	for(int i=0; i<256; i++)
	{
		if ( port_data.sock_fds[i] >= 0 )
		{
			close( port_data.sock_fds[i] );

			if ( ! addr_detected )
			{
				char tmp_addr[20];
				sprintf(tmp_addr, "%s%d", port_data.ip_range, i);

				uint16_t val = 0;
				uint16_t offset = 5; //project id reg in SmartLightingController project
				read_modbus_register(tmp_addr, 502, offset, &val);

				if ( val == 0x4433 )
				{
					sprintf(p_addr_controller, "%s%d", port_data.ip_range, i);
					addr_detected = 1;
					status = 0;
				}
			}
		}
	}

	return status;
}

/*
 * Get the controller ip address
 */
char* get_controller_addr(char* p_cached_addr)
{
	if ( strlen(p_cached_addr) == 0 )
	{
		int status = find_controller_addr(p_cached_addr);
		if ( status < 0 )
		{
			fprintf(stderr, "Failed to find controller\n");
			return NULL;
		}
		else
		{
			printf("Found controller %s\n", p_cached_addr);
		}
	}
	else
	{
		printf("Using cached controller %s\n", p_cached_addr);
	}

	return p_cached_addr;
}

static void toggle_light(GtkWidget *button, gpointer data)
{
	(void) button; /* unused */

	int index = (int)(long) data;

	if( index < 0 || index >= INPUT_COUNT )
		return;

	char *p_addr = get_controller_addr(g_ip_addr_controller);

	uint16_t offset = 6 + index;

	int status = write_modbus_register(p_addr, 502, offset, 1);

	printf("Toggled register %d with status %d\n", index, status);
}

void create_window(GtkApplication *app)
{
	GtkWidget *window = gtk_application_window_new (app);
	gtk_window_set_title (GTK_WINDOW (window), "Lighting Control");
	gtk_window_set_default_size (GTK_WINDOW (window), 800, 600);

	GtkWidget *box = gtk_flow_box_new ();
	gtk_window_set_child (GTK_WINDOW (window), box);

	for(int i=0; i<INPUT_COUNT; i++)
	{
		const char* name = get_input_name(i);

		if ( name )
		{
			GtkWidget *button = gtk_button_new_with_label (name);
			g_signal_connect (button, "clicked", G_CALLBACK (toggle_light), (gpointer)(long)i);
			gtk_flow_box_append ( (GtkFlowBox*)box, button );
		}
	}

	gtk_widget_set_visible (window, true);

	get_controller_addr(g_ip_addr_controller);
}

static void activate (GtkApplication *app, gpointer user_data)
{
	(void) user_data; /* unused */

	GList *list = gtk_application_get_windows (app);

	if (list)
		gtk_window_present (GTK_WINDOW (list->data));
	else
		create_window(app);
}

int main(int argc, char *argv[])
{
	GtkApplication* app = gtk_application_new ("org.shiftee.lightkeeper", G_APPLICATION_DEFAULT_FLAGS);
	g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
	int status = g_application_run (G_APPLICATION (app), argc, argv);
	g_object_unref (app);

	return status;
}

